#!/usr/bin/env python
# encoding: utf-8

#Copyright (c) 2021 Ryan Dela Rosa Salvador and St. Camillus MedHaven Nursing Home

#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:

#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.

#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.

import npyscreen, curses, pickle, subprocess

DAY_OF_WEEK = ['All', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
MONTH = ['All', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
HELP = ['\n','NOTE:\n', '\tMinute: 0.0 means every minute', '\tHour: 0.0 means every minute', '\tDay of month: 0.0 means everyday']
DESTINATION = '/home/pi/Music/'
        
d = {}

class SchedulesSelectOne(npyscreen.ActionPopup):
    def load_sched(self):
        global d
        f = open('data.pkl', 'rb')
        d = pickle.load(f)
        f.close()
        if d:
            main = self.parentApp.main
            main.label.value = key = str(self.schedules.values[self.schedules.value[0]])
            main.minute.value = d[key][0]
            main.hour.value = d[key][1]
            main.day_of_month.value = d[key][2]
            main.month.value = d[key][3]
            main.day_of_week.value = d[key][4]
            main.filename.value = d[key][5]
		
        self.parentApp.switchFormPrevious()
        
    def on_ok(self):
        load = npyscreen.notify_yes_no(f'Any unsaved data will be lost! Are you sure you want to load data?',
                               title='Clear form', form_color='STANDOUT', wrap=True, editw=0)
        if load:
            self.load_sched()
        
    def on_cancel(self):
        self.parentApp.switchFormPrevious()

    def create(self):
        self.schedules = self.add(npyscreen.TitleSelectOne, max_height=6, value=[0,], name="Schedules:", scroll_exit=True)	   

class DeleteSched(SchedulesSelectOne):
    def on_ok(self):
        global d
        if d:
            delete = npyscreen.notify_yes_no(f'Are you sure you want to delete {self.schedules.values[self.schedules.value[0]]}?')
            if delete:
                k = str(self.schedules.values[self.schedules.value[0]])
                schedule = d.pop(k, None)
                if not schedule:
                    self.whenDisplayText('Schedule entry not found!')
                else:
                    f = open('data.pkl', 'wb')
                    pickle.dump(d, f)
                    f.close()
                    npyscreen.notify_confirm(f'{k} has been successfully deleted!')
                    self.parentApp.main.flush_to_cron()
        self.parentApp.switchFormPrevious()
		        
class AudioSchedForm(npyscreen.ActionFormWithMenus):
    def get_schedule_info(self):
        day_of_month = 'All' if int(self.day_of_month.value) == 0 else int(self.day_of_month.value)
        return f'{self.label.value}\n \
                 Minute: {int(self.minute.value)}\n \
                 Hour: {int(self.hour.value)}\n \
                 Day of month: {day_of_month}\n \
                 Month: {MONTH[self.month.value[0]]}\n \
                 Day of week: {DAY_OF_WEEK[self.day_of_week.value[0]]}\n \
                 Filename: {self.filename.value}\n'
		
    def translate_to_cron_line(self, lst):
        if lst[0] == 60.0:
            minute = '*'
        else:
            minute = str(int(lst[0]))            
        if lst[1] == 24.0:
            hour = '*'
        else:
            hour = str(int(lst[1]))            
        if lst[2] ==  0.0:
            day_of_month = '*'
        else:
            day_of_month = str(int(lst[2]))            
        if lst[3] == 0:
            month = '*'
        else:
            month = str(lst[3])            
        if lst[4] == 0:
            day_of_week = '*'
        else:
            day_of_week = str(lst[4] - 1)            
        filename  = str(lst[5])
        return f'{minute} {hour} {day_of_month} {month} {day_of_week} pi cvlc {filename}\n'
        
    def flush_to_cron(self):
        f = open('mycronjob', 'w')
        global d
        for k in d.keys():
            line = self.translate_to_cron_line(d[k])
            f.write(line)
        f.close()
        subprocess.call(['sudo', 'cp', 'mycronjob', '/etc/cron.d/'])
		
    def on_ok(self):
        self.exit_application()
		
    def create(self):  
        self.label  = self.add(npyscreen.TitleText, name = "Label:")    
        self.minute = self.add(npyscreen.TitleSlider, step=1, out_of=60, name = "Minute:")
        self.hour = self.add(npyscreen.TitleSlider, step=1, out_of=24, name = "Hour:")
        self.day_of_month = self.add(npyscreen.TitleSlider, step=1, out_of=31, name = "Day of month:")
        self.month = self.add(npyscreen.TitleSelectOne, max_height=6, value=[0,], name='Month:',
                values=MONTH, scroll_exit=True)
        self.day_of_week = self.add(npyscreen.TitleSelectOne, max_height=6, value=[0,], name='Day of the week:',
                values=DAY_OF_WEEK, scroll_exit=True)
        self.filename = self.add(npyscreen.TitleFilenameCombo, name="Audio filename:")   
        self.how_exited_handers[npyscreen.wgwidget.EXITED_ESCAPE]  = self.exit_application    

        # The menus are created here.
        self.m1 = self.add_menu(name="Main Menu", shortcut="^M")
        self.m1.addItemsFromList([
            #("Display Text", self.whenDisplayText, None, None, ('some text..',)),
            ("<-- Back", self.whenMenuCancel),
            ("Clear", self.whenClear, "c"),
            ("Load entry", self.whenLoadEntry, "l"),
            ("Save entry",   self.whenSaveEntry, "s"),
            ("Delete entry",   self.whenDeleteEntry, "d"),
            ("Kill audio", self.whenKillAudio, "k"),
            ("Exit Application", self.exit_application, "e"),
        ])
        self.help = self.add(npyscreen.Pager, form_color='IMPORTANT', values=HELP)
        
    def whenClear(self, notify=True, clear=True):
        if notify:
            clear = npyscreen.notify_yes_no(f'Any unsaved data will be lost! Are you sure you want to clear form?',
                               title='Clear form', form_color='STANDOUT', wrap=True, editw=0)
        if clear:
            self.minute.value = self.hour.value = self.day_of_month.value = 0
            self.month.value = self.day_of_week.value = [0]
            self.label.value = self.filename.value = None
        
    def whenLoadEntry(self):
        global d
        f = open('data.pkl', 'rb')
        d = pickle.load(f)
        f.close()
        self.parentApp.scheds.schedules.values = list(d.keys())
        self.parentApp.setNextForm('scheds')
        self.parentApp.switchFormNow()

    def whenSaveEntry(self):
        l = []
        global d
        l.append(int(self.minute.value))
        l.append(int(self.hour.value))
        l.append(int(self.day_of_month.value))
        l.append(self.month.value[0])
        l.append(self.day_of_week.value[0])
        l.append(str(self.filename.value))
        d[str(self.label.value)] = l
        self.parentApp.scheds.schedules.values = list(d.keys())
        f = open('data.pkl', 'wb')
        pickle.dump(d, f)
        f.close()
        self.flush_to_cron()
        day_of_month = 'All' if int(self.day_of_month.value) == 0 else int(self.day_of_month.value)
        self.whenDisplayText(f'{self.get_schedule_info()}\n \
                              has been saved! \
                             ')
        self.whenClear(notify=False)
        
    def whenDeleteEntry(self):
        global d
        f = open('data.pkl', 'rb')
        d = pickle.load(f)
        f.close()
        self.parentApp.deletesched.schedules.values = list(d.keys())
        self.parentApp.setNextForm('deletesched')
        self.parentApp.switchFormNow()

    def whenDisplayText(self, argument):
        npyscreen.notify_confirm(argument)

    def exit_application(self):
        curses.beep()
        global d
        f = open('data.pkl', 'wb')
        pickle.dump(d, f)
        f.close()
        self.flush_to_cron()
        self.parentApp.setNextForm(None)
        self.editing = False
        self.parentApp.switchFormNow()
        
    def whenMenuCancel(self):
        self.parentApp.switchFormPrevious()
        
    def whenKillAudio(self):
        subprocess.call(['pkill', 'vlc'])

class MyApp(npyscreen.NPSAppManaged):
    def onStart(self):
        #npyscreen.setTheme(npyscreen.Themes.ColorfulTheme)
        global d
        try:
            f = open('data.pkl', 'rb')
        except IOError:
            f = open('data.pkl', 'wb')
            pickle.dump(d, f)
        finally:
            f.close()    
        self.scheds = SchedulesSelectOne(name='Schedules')
        self.main = AudioSchedForm(name='Welcome to JHWC Audio Scheduler')
        self.deletesched = DeleteSched(name='Delete Schedule')
        self.registerForm('MAIN', self.main)
        self.registerForm('scheds', self.scheds)
        self.registerForm('deletesched', self.deletesched)

if __name__ == '__main__':
    myApp = MyApp().run()

